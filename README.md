# treemenu-sample
## Getting started

First you need to clone the project. To do this, open IntelliJ IDEA

>File -> New -> Project from Version Control...

In openned window paste https://git.haulmont.com/bespoke/ats/ats-psz.git to URL filed and push Clone.

To build the project you need to add file **gradle.properties** to root project's folder.
###### gradle.properties
``` INI
repoUser=ats-view
repoPass=???
repoUrl=https://repository-custom.haulmont.com/repository/ats-maven/
```
You can get **repoPass** param from https://pass.haulmont.com/#!/p/61b9d12d2ccf98749d412bbf

Than you can build the project. If you saw the inscription BUILD SUCCESSFUL - you are breathtaking!

## Minimum working functionality

For beginning you can start the application without using Docker to check that the project can start with minimum working functionality. All you need is an IntelliJ IDEA and a PostgeSQL.

You need to create file **application-custom.properties**. 

> /ats-psz/ats-psz-app/scr/main/resources/application-custom.properties

Later, you will start the application with the spring profile "custom" and all the settings specified in this file will replace the default settings (application.properties).

###### application-custom.properties
``` INI
# local storage
jmix.core.default-file-storage=fs

# db settings
main.datasource.url=jdbc:postgresql://localhost:5432/psz
main.datasource.username=???
main.datasource.password=???

# keycloak
keycloak.clientId=psz
keycloak.clientSecret=???
keycloak.redirectUri=http://localhost:8080/login/oauth2/code/
keycloak.postLogoutUri=http://localhost:8080/
keycloak.realm=ATS
keycloak.apiUser=api-access-tech-user
keycloak.apiPassword=???
keycloak.serverUrl=???
keycloak.requiredUserGroup=PSZ
keycloak.authorizationUri=${keycloak.serverUrl}/realms/ATS/protocol/openid-connect/auth
keycloak.jwkSetUri=${keycloak.serverUrl}/realms/ATS/protocol/openid-connect/certs
keycloak.userInfoUri=${keycloak.serverUrl}/realms/ATS/protocol/openid-connect/userinfo
keycloak.tokenUri=${keycloak.serverUrl}/realms/ATS/protocol/openid-connect/token
keycloak.issuerUri=${keycloak.serverUrl}/realms/ATS
keycloak.endSessionUri=${keycloak.serverUrl}/realms/ATS/protocol/openid-connect/logout
keycloak.introspectionUri=${keycloak.serverUrl}/realms/ATS/protocol/openid-connect/token/introspect
keycloak.revocationUri=${keycloak.serverUrl}/realms/ATS/protocol/openid-connect/revoke

# smtp settings
spring.mail.host=localhost
spring.mail.port=25
spring.mail.protocol=smtp
spring.mail.username=
spring.mail.password=
spring.mail.properties.mail.smtp.auth=false
spring.mail.properties.mail.smtp.starttls.enable=false
jmix.email.from-address=ats@haulmont.dev
```

### # local storage

We use minio storage, but now you can use local storage *fs* to simplify the process. Also to use local storage you need fix **build.gradle**

>  /ats-psz/ats-psz-app/build.gradle

Find and comment out this code block:
``` Gradle
implementation('com.ats:minio-starter:0.0.1') {
        exclude group: 'org.bouncycastle', module: 'bcprov-jdk15on'
    }
```
Also you need to add here new implementation

``` Gradle
implementation 'io.jmix.localfs:jmix-localfs-starter'

```

### # db settings

Create new database **psz** on your localhost. Correctly specify username and password from your local database

### # keycloak

This is an easy way to connect keycloak - connect to an external service. 

 - **clientSecret** - you can get it from https://pass.haulmont.com/#!/p/64120fa337724f91fc0f1e96 (field "Password")

 - **apiPassword** - ask the teamlead about it 

 - **serverUrl** -  
    + If you have access to the Haulmont internal network use http://keycloak.ats-d.kube.haulmont.com 
    + Else use external https://ats-keycloak.demo.haulmont.com/

### # smtp settings

Maybe you don't need smtp-server on this point, but without that setting the project can't starts)

Now you can run the project. At first check you SDK version.

> File -> Project Structure -> Project

You need to set 11 version. Also remember the way to your jdk folder. You'll need it later.

Than you need to create a new Run Configuration. To the left of the Run and Debug buttons, open the drop-down list and select "Edit Configurations...". In opened window push the "+" button in the upper left corner and select "Application". Set this settings:

 - **JDK** - java 11
 - **-cp** - ats-psz.ats-psz-app.main
 - **Main class** - com.ats.psz.AtsPszApplication
 - **Enviroment variables** - spring.profiles.active=dev

 

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/anya-ch99/treemenu-sample.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/anya-ch99/treemenu-sample/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
