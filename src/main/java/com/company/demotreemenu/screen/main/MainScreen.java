package com.company.demotreemenu.screen.main;

import com.company.demotreemenu.builder.MenuBuilder;
import com.company.demotreemenu.entity.MenuItemDTO;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.shared.ui.grid.DropLocation;
import com.vaadin.shared.ui.grid.DropMode;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.components.grid.TreeGridDragSource;
import com.vaadin.ui.components.grid.TreeGridDropTarget;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.ui.AbstractSelect;
import io.jmix.core.LoadContext;
import io.jmix.core.MessageTools;
import io.jmix.core.Messages;
import io.jmix.ui.Notifications;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.ScreenTools;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.component.*;
import io.jmix.ui.component.mainwindow.Drawer;
import io.jmix.ui.icon.JmixIcon;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.navigation.Route;
import io.jmix.ui.screen.*;
import io.jmix.ui.widget.JmixTree;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.anna.dndscroll.TableAutoScrollExtension;

import java.util.*;

@UiController("MainScreen")
@UiDescriptor("main-screen.xml")
@Route(path = "main", root = true)
public class MainScreen extends Screen implements Window.HasWorkArea {
    private static final Logger log = LoggerFactory.getLogger(MainScreen.class);

    @Autowired
    private ScreenTools screenTools;

    @Autowired
    private AppWorkArea workArea;
    @Autowired
    private Drawer drawer;
    @Autowired
    private Button collapseDrawerButton;
    @Autowired
    private MenuBuilder menuBuilder;
    @Autowired
    private CollectionLoader<MenuItemDTO> menuItemDToesDl;
    @Autowired
    private MessageBundle messageBundle;
    @Autowired
    private MessageTools messageTools;
    @Autowired
    private Messages messages;
    @Autowired
    private TreeTable<MenuItemDTO> menuTreeTable;
    @Autowired
    private CollectionContainer<MenuItemDTO> menuItemDToesDc;
    @Autowired
    private Notifications notifications;
    @Autowired
    private TreeDataGrid<MenuItemDTO> menuTreeGrid;
    @Autowired
    private Tree<MenuItemDTO> menuTree;
    @Autowired
    private ScreenBuilders screenBuilders;


    @Override
    public AppWorkArea getWorkArea() {
        return workArea;
    }

    @Subscribe("collapseDrawerButton")
    private void onCollapseDrawerButtonClick(Button.ClickEvent event) {
        drawer.toggle();
        if (drawer.isCollapsed()) {
            collapseDrawerButton.setIconFromSet(JmixIcon.CHEVRON_RIGHT);
        } else {
            collapseDrawerButton.setIconFromSet(JmixIcon.CHEVRON_LEFT);
        }
    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        screenTools.openDefaultScreen(
                UiControllerUtils.getScreenContext(this).getScreens());

        screenTools.handleRedirect();
        menuItemDToesDl.load();
        dragAndDropTreeTable();
        dragAndDropTreeGrid();
        dragAndDropTree();
        addContextMenuTable();
        addContextMenuGrid();
        addContextMenuTree();
        addClickListenerTable();
        addClickListenerTreeGrid();
        addClickListenerTree();
    }

    @Subscribe("menuTreeTable")
    public void onMenuTreeTableColumnCollapse(Table.ColumnCollapseEvent<MenuItemDTO> event) {
        int i = 0;
    }



    private void addClickListenerTable(){
        com.vaadin.v7.ui.TreeTable unwrapTreeTable = menuTreeTable.unwrap(com.vaadin.v7.ui.TreeTable.class);
        unwrapTreeTable.addItemClickListener(itemClick -> {
            MouseEventDetails mouseEventDetails = itemClick.getMouseEventDetails();
            if (mouseEventDetails.getButton().equals(MouseEventDetails.MouseButton.LEFT) && mouseEventDetails.isDoubleClick()){
                Object itemId = itemClick.getItemId();
                MenuItemDTO item = menuItemDToesDc.getItem(itemId);
                if (item.getIsMenu()) {
                    boolean expanded = menuTreeTable.isExpanded(itemId);
                    if (expanded) {
                        menuTreeTable.collapse(itemId);
                    } else {
                        menuTreeTable.expand(itemId);
                    }
                } else {
                    screenBuilders.screen(this).withScreenId(item.getScreen()).build().show();
                }
            }
        });
    }

    private void addClickListenerTree(){
        com.vaadin.ui.Tree<MenuItemDTO> unwrapTree = menuTree.unwrap(com.vaadin.ui.Tree.class);
        unwrapTree.addItemClickListener(itemClick -> {
            MouseEventDetails mouseEventDetails = itemClick.getMouseEventDetails();
            if (mouseEventDetails.getButton().equals(MouseEventDetails.MouseButton.LEFT) &&
                    mouseEventDetails.isDoubleClick()){
                MenuItemDTO item = itemClick.getItem();
                if (item.getIsMenu()) {
                    boolean expanded = menuTree.isExpanded(item);
                    if (expanded) {
                        menuTree.collapse(item);
                    } else {
                        menuTree.expand(item);
                    }
                } else {
                    screenBuilders.screen(this).withScreenId(item.getScreen()).build().show();
                }
            }
        });
    }

    private void addClickListenerTreeGrid(){
        com.vaadin.ui.TreeGrid<MenuItemDTO> unwrapTreeGrid = menuTreeGrid.unwrap(com.vaadin.ui.TreeGrid.class);
        unwrapTreeGrid.addItemClickListener(itemClick -> {
            MouseEventDetails mouseEventDetails = itemClick.getMouseEventDetails();
            if (mouseEventDetails.getButton().equals(MouseEventDetails.MouseButton.LEFT) &&
                    mouseEventDetails.isDoubleClick()){
                MenuItemDTO item = itemClick.getItem();
                if (item.getIsMenu()) {
                    boolean expanded = menuTreeGrid.isExpanded(item);
                    if (expanded) {
                        menuTreeGrid.collapse(item);
                    } else {
                        menuTreeGrid.expand(item);
                    }
                } else {
                    screenBuilders.screen(this).withScreenId(item.getScreen()).build().show();
                }
            }
        });
    }

    private void addContextMenuTable() {
        menuTreeTable.addAction(new SampleAction("sampleAction", menuTreeTable));
        menuTreeTable.addAction(new SampleAction1("sampleAction1", menuTreeTable));
    }

    private void addContextMenuGrid() {
        menuTreeGrid.addAction(new SampleAction("sampleAction", menuTreeGrid));
        menuTreeGrid.addAction(new SampleAction1("sampleAction1", menuTreeGrid));
    }

    private void addContextMenuTree() {
        menuTree.addAction(new SampleAction("sampleAction", menuTree));
        menuTree.addAction(new SampleAction1("sampleAction1", menuTree));
    }

    private void dragAndDropTree() {

        JmixTree<MenuItemDTO> unwrap = menuTree.unwrap(JmixTree.class);
        TreeGridDragSource<MenuItemDTO> menuItemDTOTreeGridDragSource = new TreeGridDragSource<>(unwrap.getCompositionRoot());
        menuItemDTOTreeGridDragSource.setDragDataGenerator("id", MenuItemDTO::getId);

        TreeGridDropTarget<MenuItemDTO> dropTarget = new TreeGridDropTarget<>(unwrap.getCompositionRoot(), DropMode.ON_TOP);
        dropTarget.setDropMode(DropMode.ON_TOP_OR_BETWEEN);
        dropTarget.addTreeGridDropListener(event -> {
            DropLocation dropLocation = event.getDropLocation();
            String id = event.getDataTransferData().get("id");
            if (id != null && menuItemDToesDc.containsItem(id)){
                MenuItemDTO draggedItem = menuItemDToesDc.getItem(id);
                event.getDropTargetRow().ifPresent(dropTargetItem -> {
                    String formatStr = String.format("%s %%s %s", getMenuItemCaption(draggedItem), getMenuItemCaption(dropTargetItem));
                    String message = "";
                    switch (dropLocation){
                        case ABOVE -> message = String.format(formatStr, "over");
                        case BELOW -> message = String.format(formatStr, "under");
                        case ON_TOP -> message = String.format(formatStr, "on");
                    }
                    notifications.create().withCaption(message).show();
                });
            }
        });
    }
    private void dragAndDropTree2() {

        JmixTree<MenuItemDTO> unwrap = menuTree.unwrap(JmixTree.class);
        com.vaadin.ui.Tree<MenuItemDTO> unwrap1 = menuTree.unwrap(com.vaadin.ui.Tree.class);
        TreeGridDragSource<MenuItemDTO> menuItemDTOTreeGridDragSource = new TreeGridDragSource<>(unwrap.getCompositionRoot());
        menuItemDTOTreeGridDragSource.setDragDataGenerator("id", MenuItemDTO::getId);

        TreeGridDropTarget<MenuItemDTO> dropTarget = new TreeGridDropTarget<>(unwrap.getCompositionRoot(), DropMode.ON_TOP);
        dropTarget.setDropMode(DropMode.ON_TOP_OR_BETWEEN);
        dropTarget.addTreeGridDropListener(event -> {
            DropLocation dropLocation = event.getDropLocation();
            String id = event.getDataTransferData().get("id");
            if (id != null && menuItemDToesDc.containsItem(id)){
                MenuItemDTO draggedItem = menuItemDToesDc.getItem(id);
                event.getDropTargetRow().ifPresent(dropTargetItem -> {
                    String formatStr = String.format("%s %%s %s", getMenuItemCaption(draggedItem), getMenuItemCaption(dropTargetItem));
                    String message = "";
                    switch (dropLocation){
                        case ABOVE -> message = String.format(formatStr, "over");
                        case BELOW -> message = String.format(formatStr, "under");
                        case ON_TOP -> message = String.format(formatStr, "on");
                    }
                    notifications.create().withCaption(message).show();
                });
            }
        });
    }

    private void dragAndDropTreeGrid() {

        TreeGrid<MenuItemDTO> unwrap = menuTreeGrid.unwrap(TreeGrid.class);
        TreeGridDragSource<MenuItemDTO> menuItemDTOTreeGridDragSource = new TreeGridDragSource<>(unwrap);
        menuItemDTOTreeGridDragSource.setDragDataGenerator("id", MenuItemDTO::getId);

        TreeGridDropTarget<MenuItemDTO> dropTarget = new TreeGridDropTarget<>(unwrap, DropMode.ON_TOP);
        dropTarget.setDropMode(DropMode.ON_TOP_OR_BETWEEN);
        dropTarget.addTreeGridDropListener(event -> {
            DropLocation dropLocation = event.getDropLocation();
            String id = event.getDataTransferData().get("id");
            if (id != null && menuItemDToesDc.containsItem(id)){
                MenuItemDTO draggedItem = menuItemDToesDc.getItem(id);
                event.getDropTargetRow().ifPresent(dropTargetItem -> {
                    String formatStr = String.format("%s %%s %s", getMenuItemCaption(draggedItem), getMenuItemCaption(dropTargetItem));
                    String message = "";
                    switch (dropLocation){
                        case ABOVE -> message = String.format(formatStr, "over");
                        case BELOW -> message = String.format(formatStr, "under");
                        case ON_TOP -> message = String.format(formatStr, "on");
                    }
                    notifications.create().withCaption(message).show();
                });
            }
        });
    }


    private void dragAndDropTreeTable() {
        com.vaadin.v7.ui.TreeTable unwrapTreeTable = menuTreeTable.unwrap(com.vaadin.v7.ui.TreeTable.class);

        TableAutoScrollExtension extension = new TableAutoScrollExtension();
        extension.extend(unwrapTreeTable);
        unwrapTreeTable.setDragMode(com.vaadin.v7.ui.Table.TableDragMode.ROW);
        unwrapTreeTable.setDropHandler(new TreeDropHandler());
    }

    protected class TreeDropHandler implements DropHandler {

        @Override
        public void drop(DragAndDropEvent event) {
            Object itemId = event.getTransferable().getData("itemId");
            AbstractSelect.AbstractSelectTargetDetails dropTargetData =
                    (AbstractSelect.AbstractSelectTargetDetails) event.getTargetDetails();

            MenuItemDTO draggedItem;
            MenuItemDTO dropTargetItem;
            try {
                draggedItem = menuItemDToesDc.getItem(itemId);
                dropTargetItem = menuItemDToesDc.getItem(dropTargetData.getItemIdOver());
            } catch (IllegalArgumentException e) {
                log.info(e.getLocalizedMessage());
                return;
            }

            VerticalDropLocation dropLocation = dropTargetData.getDropLocation();
            String formatStr = String.format("%s %%s %s", getMenuItemCaption(draggedItem), getMenuItemCaption(dropTargetItem));
            String message = "";
            if (VerticalDropLocation.MIDDLE.equals(dropLocation)) {
                message = String.format(formatStr, "on");
            } else if (VerticalDropLocation.TOP.equals(dropLocation)) {
                message = String.format(formatStr, "over");
            } else if (VerticalDropLocation.BOTTOM.equals(dropLocation)) {
                message = String.format(formatStr, "under");
            }
            notifications.create().withCaption(message).show();
        }

        @Override
        public AcceptCriterion getAcceptCriterion() {
            return AcceptAll.get();
        }
    }

    @Install(to = "menuItemDToesDl", target = Target.DATA_LOADER)
    private List<MenuItemDTO> menuItemDToesDlLoadDelegate(LoadContext<MenuItemDTO> loadContext) {
        return menuBuilder.getMenuItemsDTO();
    }

    @Install(to = "menuTreeTable.caption", subject = "valueProvider")
    private Object menuTreeCaptionValueProvider(MenuItemDTO menuItemDTO) {
        return getMenuItemCaption(menuItemDTO);
    }

    private String getMenuItemCaption(MenuItemDTO menuItemDTO) {
        String caption = menuItemDTO.getCaption();
        if (StringUtils.isNotEmpty(caption)) {
            String localizedCaption = messageTools.loadString(caption);
            if (StringUtils.isNotEmpty(localizedCaption)) {
                return localizedCaption;
            }
        }
        String id = menuItemDTO.getId();
        return messages.getMessage("menu-config." + id);
    }

    @Install(to = "menuTreeGrid.caption", subject = "columnGenerator")
    private Object menuTreeGridCaptionColumnGenerator(DataGrid.ColumnGeneratorEvent<MenuItemDTO> columnGeneratorEvent) {
        return getMenuItemCaption(columnGeneratorEvent.getItem());
    }

    @Install(to = "menuTree", subject = "itemCaptionProvider")
    private String menuTreeItemCaptionProvider(MenuItemDTO menuItemDTO) {
        return getMenuItemCaption(menuItemDTO);
    }

    private class SampleAction extends BaseAction {
        {
            setCaption(messageBundle.getMessage("menuTreeTable.sampleAction.caption"));
        }

        private ListComponent<MenuItemDTO> targetComponent;

        public SampleAction(String id, ListComponent<MenuItemDTO> targetComponent) {
            super(id);
            this.targetComponent = targetComponent;
        }

        @Override
        public void actionPerform(Component component) {
            notifications.create().withCaption("SampleAction").show();
        }

        @Override
        protected boolean isApplicable() {
            Set<MenuItemDTO> selected = targetComponent.getSelected();
            boolean isApplicable = selected.size() == 1 && !selected.iterator().next().getIsMenu();
            return isApplicable;
        }

        @Override
        public void refreshState() {
            super.refreshState();
            setVisibleInternal(isVisible() && isEnabled());
        }
    }

    private class SampleAction1 extends BaseAction {
        {
            setCaption(messageBundle.getMessage("menuTreeTable.sampleAction1.caption"));
        }

        private ListComponent<MenuItemDTO> targetComponent;

        public SampleAction1(String id, ListComponent<MenuItemDTO> targetComponent) {
            super(id);
            this.targetComponent = targetComponent;
        }

        @Override
        public void actionPerform(Component component) {
            notifications.create().withCaption("SampleAction1").show();
        }
    }
}
