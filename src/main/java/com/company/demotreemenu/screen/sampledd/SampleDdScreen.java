package com.company.demotreemenu.screen.sampledd;

import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.server.Sizeable;
import com.vaadin.shared.ui.dd.HorizontalDropLocation;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.*;
import com.vaadin.v7.data.util.BeanItemContainer;
import com.vaadin.v7.ui.AbstractSelect;
import com.vaadin.v7.ui.Table;
import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import org.vaadin.anna.dndscroll.PanelAutoScrollExtension;
import org.vaadin.anna.dndscroll.TableAutoScrollExtension;

@UiController("SampleDdScreen")
@UiDescriptor("sample-dd-screen.xml")
public class SampleDdScreen extends Screen {

    @Subscribe
    public void onInit(InitEvent event) {
        initLayout();
    }

    private void initLayout() {
        Layout unwrap = getWindow().unwrap(Layout.class);
        final VerticalLayout layout = new VerticalLayout();
        layout.setStyleName("demoContentLayout");
        layout.setSizeUndefined();
        layout.setSpacing(true);
        layout.setMargin(true);
        layout.addComponents(createTable(), createHorizontalPanel(),
                createVerticalPanel());

        unwrap.addComponent(layout);
    }

    protected Table createTable() {
        Table table = new Table();
        BeanItemContainer<TestBean> container = new BeanItemContainer<TestBean>(
                TestBean.class);
        for (int i = 0; i < 30; ++i) {
            container.addBean(new TestBean("item" + i));
        }
        table.setDragMode(Table.TableDragMode.ROW);
        table.setContainerDataSource(container);
        table.setPageLength(6);
        table.setWidth(200, Sizeable.Unit.PIXELS);
        table.setDropHandler(new TableDropHandler());
        TableAutoScrollExtension extension = new TableAutoScrollExtension();
        extension.extend(table);
        return table;
    }

    private Component createHorizontalPanel() {
        Panel panel = new Panel();
        HorizontalLayout content = new HorizontalLayout();
        content.setSpacing(true);
        PanelDropHandler dropHandler = new PanelDropHandler(content);
        for (int i = 0; i < 30; ++i) {
            Label label = new Label("label" + i);
            label.setWidthUndefined();
            DragAndDropWrapper wrapper = new DragAndDropWrapper(label);
            wrapper.setDragStartMode(DragAndDropWrapper.DragStartMode.COMPONENT);
            wrapper.setDropHandler(dropHandler);
            content.addComponent(wrapper);
        }
        panel.setContent(content);
        panel.setWidth(200, Sizeable.Unit.PIXELS);
        PanelAutoScrollExtension extension = new PanelAutoScrollExtension();
        extension.extend(panel);
        return panel;
    }

    private Component createVerticalPanel() {
        Panel panel = new Panel();
        VerticalLayout content = new VerticalLayout();
        content.setSpacing(true);
        PanelDropHandler dropHandler = new PanelDropHandler(content);
        for (int i = 0; i < 30; ++i) {
            Label label = new Label("label" + i);
            label.setWidthUndefined();
            DragAndDropWrapper wrapper = new DragAndDropWrapper(label);
            wrapper.setDragStartMode(DragAndDropWrapper.DragStartMode.COMPONENT);
            wrapper.setDropHandler(dropHandler);
            content.addComponent(wrapper);
        }
        panel.setContent(content);
        panel.setHeight(200, Sizeable.Unit.PIXELS);
        PanelAutoScrollExtension extension = new PanelAutoScrollExtension();
        extension.extend(panel);
        return panel;
    }

    protected static class PanelDropHandler implements DropHandler {
        private AbstractOrderedLayout content;

        public PanelDropHandler(AbstractOrderedLayout content) {
            this.content = content;
        }

        @Override
        public void drop(DragAndDropEvent event) {
            DragAndDropWrapper.WrapperTargetDetails details = (DragAndDropWrapper.WrapperTargetDetails) event
                    .getTargetDetails();
            if (!(event.getTransferable() instanceof DragAndDropWrapper.WrapperTransferable)) {
                Notification.show("illegal drop");
                return;
            }
            DragAndDropWrapper.WrapperTransferable transferable = (DragAndDropWrapper.WrapperTransferable) event
                    .getTransferable();

            Component target = (details.getTarget());
            while (!(target instanceof DragAndDropWrapper)
                    && target.getParent() != null) {
                if (target == content) {
                    break;
                }
                target = target.getParent();
            }

            Component draggedComponent = transferable.getDraggedComponent();
            Component parent = draggedComponent.getParent();
            while (!content.equals(parent) && parent != null) {
                draggedComponent = parent;
                parent = parent.getParent();
            }
            if (!content.equals(parent)) {
                Notification.show("illegal drop");
                return;
            }

            content.removeComponent(draggedComponent);
            if (VerticalDropLocation.TOP.equals(details
                    .getVerticalDropLocation())
                    || HorizontalDropLocation.LEFT.equals(details
                    .getHorizontalDropLocation())) {
                content.addComponent(draggedComponent,
                        content.getComponentIndex(target));
            } else {
                content.addComponent(draggedComponent,
                        content.getComponentIndex(target) + 1);
            }
        }

        @Override
        public AcceptCriterion getAcceptCriterion() {
            return AcceptAll.get();
        }

    }

    protected static class TableDropHandler implements DropHandler {

        public TableDropHandler() {
        }

        @Override
        public void drop(DragAndDropEvent event) {
            AbstractSelect.AbstractSelectTargetDetails details = (AbstractSelect.AbstractSelectTargetDetails) event
                    .getTargetDetails();
            Table.TableTransferable transferable = (Table.TableTransferable) event
                    .getTransferable();

            Table table = (Table) details.getTarget();

            Object itemId = transferable.getItemId();
            table.removeItem(itemId);
            if (VerticalDropLocation.TOP.equals(details.getDropLocation())) {
                @SuppressWarnings("unchecked")
                BeanItemContainer<TestBean> container = (BeanItemContainer<TestBean>) table
                        .getContainerDataSource();
                TestBean prevItemId = container.prevItemId(details
                        .getItemIdOver());
                if (prevItemId != null) {
                    container.addItemAfter(prevItemId, itemId);
                } else {
                    container.addItemAt(0, itemId);
                }
            } else {
                table.addItemAfter(details.getItemIdOver(), itemId);
            }
        }

        @Override
        public AcceptCriterion getAcceptCriterion() {
            return AcceptAll.get();
        }

    }

    public static class TestBean {
        private String name;

        public TestBean(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}