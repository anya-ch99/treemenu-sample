package com.company.demotreemenu.builder;

import com.company.demotreemenu.entity.MenuItemDTO;
import io.jmix.core.DataManager;
import io.jmix.ui.menu.MenuConfig;
import io.jmix.ui.menu.MenuItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

@Component("MenuBuilder")
public class MenuBuilder {
    @Autowired
    private MenuConfig menuConfig;
    @Autowired
    private DataManager dataManager;

    public List<MenuItemDTO> getMenuItemsDTO(){
        List<MenuItemDTO> menuItemDTOList = new ArrayList<>();

        List<MenuItem> rootItems = menuConfig.getRootItems();
        for (MenuItem rootItem : rootItems) {
            getChildOfMenuItems(menuItemDTOList, rootItem, null);
        }
        return menuItemDTOList;
    }

    private void getChildOfMenuItems(List<MenuItemDTO> menuItemDTOList,
                                     MenuItem menuItem, @Nullable MenuItemDTO menuItemDTOParent){
        MenuItemDTO menuItemDTO = convertMenuItemToDTO(menuItem, menuItemDTOParent);
        menuItemDTOList.add(menuItemDTO);
        for (MenuItem childItem : menuItem.getChildren()) {
            getChildOfMenuItems(menuItemDTOList, childItem, menuItemDTO);
        }
    }

    private MenuItemDTO convertMenuItemToDTO(MenuItem menuItem, @Nullable MenuItemDTO menuItemDTOParent){
        MenuItemDTO menuItemDTO = dataManager.create(MenuItemDTO.class);
        menuItemDTO.setId(menuItem.getId());
        menuItemDTO.setCaption(menuItem.getCaption());
        menuItemDTO.setExpanded(menuItem.isExpanded());
        menuItemDTO.setScreen(menuItem.getScreen());
        menuItemDTO.setIsMenu(menuItem.isMenu());
        if (menuItemDTOParent != null) {
            menuItemDTO.setParent(menuItemDTOParent);
        }
        return menuItemDTO;
    }
}